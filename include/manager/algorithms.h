/*
 * algorithms.h
 *
 *      Author: zephyr
 */

#ifndef ALGORITHMS_H_
#define ALGORITHMS_H_

#include <vector>
#include "ns.h"
#include "model/node.h"
#include "model/relation.h"
#include "manager/manager.h"
#include "common.h"

using namespace std;

class graphdb::Algorithms {
public:
	Algorithms();
	~Algorithms();
	static vector<Node*> *DFS(const long id);
	static vector<Node*> *BFS(const long id);
	static vector<Node*> *getOutNodes(const long id);
	static vector<Node*> *getInNodes(const long id);
	static vector<Node*> *getBidirectionalNodes(const long id);

};

#endif /* ALGORITHMS_H_ */
