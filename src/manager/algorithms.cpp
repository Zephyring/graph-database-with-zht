/*
 * algorithms.cpp
 *
 *      Author: zephyr
 */

#include "manager/algorithms.h"
#include <stack>
#include <queue>

using namespace graphdb;

vector<Node*> *Algorithms::DFS(const long id)
{
	Manager *manager = Manager::getInstance();

	Node *start = manager->getNode(id);
	if(start == NULL)
		return NULL;
	else destroy(start);

	vector<Node*> *nodes = new vector<Node*>;
	map<long, bool> discovered;
	stack<long> stack;

	discovered[id] = true;
	stack.push(id);

s:	while(!stack.empty())
	{
		long v = stack.top();
		Node *node = manager->getNode(v);
		nodes->push_back(node);

		set<string> rels = node->getRelations(OUT);// get uuid of all out rels
		for(set<string>::iterator it = rels.begin(); it != rels.end(); it++)
		{
			Relation *r = manager->getRelation(Relation::getId(*it));
			long endNode = Node::getId(r->getEndNode());
			if(discovered[endNode] != true)
			{
				discovered[endNode] = true;
				stack.push(endNode);
				nodes->push_back(manager->getNode(endNode));
				destroy(r);
				goto s;
			}
		}
		stack.pop();
	}

	return nodes;
}

vector<Node*> *Algorithms::BFS(const long id)
{
	Manager *manager = Manager::getInstance();

	Node *start = manager->getNode(id);
	if(start == NULL)
		return NULL;
	else destroy(start);

	vector<Node*> *nodes = new vector<Node*>;
	map<long, bool> discovered;
	queue<long> q;

	q.push(id);
	discovered[id] = true;

	while(!q.empty())
	{
		long v = q.front();
		q.pop();
		Node *node = manager->getNode(v);
		nodes->push_back(node);

		set<string> rels = node->getRelations(OUT);// get uuid of all out rels
		for(set<string>::iterator it = rels.begin(); it != rels.end(); it++)
		{
			Relation *r = manager->getRelation(Relation::getId(*it));
			long endNode = Node::getId(r->getEndNode());
			if(discovered[endNode] != true)
			{
				discovered[endNode] = true;
				q.push(endNode);
				nodes->push_back(manager->getNode(endNode));
				destroy(r);
			}
		}
	}
	return nodes;
}

vector<Node*> *Algorithms::getOutNodes(const long id)
{
	Manager *manager = manager->getInstance();

	Node *start = manager->getNode(id);
	if(start == NULL)
		return NULL;

	vector<Node*> *nodes = new vector<Node*>;
	set<string> rels = start->getRelations(OUT);

	for(set<string>::iterator it = rels.begin(); it != rels.end(); it++)
	{
		Relation *r = manager->getRelation(Relation::getId(*it));
		Node *n = manager->getNode(Node::getId(r->getEndNode()));
		nodes->push_back(n);
		destroy(r);
	}

	destroy(start);
	return nodes;
}

vector<Node*> *Algorithms::getInNodes(const long id)
{
	Manager *manager = manager->getInstance();

	Node *start = manager->getNode(id);
	if(start == NULL)
		return NULL;

	vector<Node*> *nodes = new vector<Node*>;
	set<string> rels = start->getRelations(IN);

	for(set<string>::iterator it = rels.begin(); it != rels.end(); it++)
	{
		Relation *r = manager->getRelation(Relation::getId(*it));
		Node *n = manager->getNode(Node::getId(r->getStartNode()));
		nodes->push_back(n);
		destroy(r);
	}

	destroy(start);
	return nodes;
}

vector<Node*> *Algorithms::getBidirectionalNodes(const long id)
{
	Manager *manager = manager->getInstance();

	Node* start = manager->getNode(id);
	if(start == NULL)
		return NULL;

	vector<Node*> *nodes = new vector<Node*>;
	set<string> outRels = start->getRelations(OUT);
	set<string> inRels = start->getRelations(IN);

	//if endnode of outrel is equal to startnode of inrel, it's undirected neighbor
	for(set<string>::iterator it = outRels.begin(); it != outRels.end(); it++)
	{
		for(set<string>::iterator iit = inRels.begin(); iit != inRels.end(); iit++)
		{
			Relation *out = manager->getRelation(Relation::getId(*it));
			Relation *in = manager->getRelation(Relation::getId(*it));

			if(out->getEndNode().compare(in->getStartNode()) == 0)
				nodes->push_back(manager->getNode(Node::getId(out->getEndNode())));

			destroy(out);
			destroy(in);
		}
	}

	destroy(start);
	return nodes;
}

