/*
 * helper.cpp
 *
 *      Author: zephyr
 */

#include "helper/helper.h"
#include "model/types.h"
#include <iostream>
#include <fstream>

using namespace std;
using namespace graphdb;

static std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems) {
    std::stringstream ss(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
        elems.push_back(item);
    }
    return elems;
}

std::vector<std::string> split(const std::string &s, char delim) {
    std::vector<std::string> elems;
    split(s, delim, elems);
    return elems;
}

std::vector<int*> *readData(string filename)
{
	vector<int*> *v = new vector<int*>;

	ifstream file;
	file.open(filename.c_str(), std::ifstream::in);

	if(!file)
	{
		cout <<"Fail to open the file" << endl;
		return NULL;
	}

	string x;
	while(file >> x)
	{
		int *a = new int[2];
		a[0] = graphdb::stringToInt(x);
		file >> x;
		a[1] = graphdb::stringToInt(x);
		v->push_back(a);
	}

	file.close();
	return v;
}


